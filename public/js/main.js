$(document).ready(function () {
    $("#query_select").on("change", function () {
        if ($(this).val()) {
            id = $("option:selected", this).data("machine");
            $('#'+id).show().siblings('form').hide();
        } else {
            $('.form').hide();
        }
    })
});