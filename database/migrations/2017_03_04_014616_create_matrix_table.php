<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatrixTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matrix', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('dimension');
            $table->integer('procedures_number');
            $table->integer('completed_procedures');
            $table->timestamps();
            $table->integer('test_case_id')->unsigned();
            $table->foreign('test_case_id')->references('id')->on('test_case')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matrix');
    }
}
