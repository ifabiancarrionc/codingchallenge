<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatrixQueryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matrix_query', function (Blueprint $table) {
            $table->increments('id');
            $table->string('result');
            $table->string("procedure");
            $table->integer('matrix_id')->unsigned();
            $table->integer('query_id')->unsigned();
            $table->foreign('matrix_id')->references('id')->on('matrix')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('query_id')->references('id')->on('query')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matrix_query');
    }
}
