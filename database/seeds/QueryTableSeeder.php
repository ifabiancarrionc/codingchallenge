<?php

use Illuminate\Database\Seeder;

class QueryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('query')->insert([
            'id' => 1,
            'name' => "UPDATE",
            'machine_name' => "update",
        ]);
        DB::table('query')->insert([
            'id' => 2,
            'name' => "QUERY",
            'machine_name' => "query",
        ]);
    }
}