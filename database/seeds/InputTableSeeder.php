<?php

use Illuminate\Database\Seeder;

class InputTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('input')->insert([
            'id' => 1,
            'name' => "x",
            'label' => "Coordinate X",
            'from_value' => 0,
            'to_value' => 0,
            'query_id' => 1,
        ]);
        DB::table('input')->insert([
            'id' => 2,
            'name' => "y",
            'label' => "Coordinate Y",
            'from_value' => 0,
            'to_value' => 0,
            'query_id' => 1,
        ]);
        DB::table('input')->insert([
            'id' => 3,
            'name' => "z",
            'label' => "Coordinate Z",
            'from_value' => 0,
            'to_value' => 0,
            'query_id' => 1,
        ]);
        DB::table('input')->insert([
            'id' => 4,
            'name' => "value",
            'label' => "Value",
            'from_value' => -pow(10, 9),
            'to_value' => pow(10, 9),
            'query_id' => 1,
        ]);
        DB::table('input')->insert([
            'id' => 5,
            'name' => "x1",
            'label' => "From Coordinate X",
            'from_value' => 0,
            'to_value' => 0,
            'query_id' => 2,
        ]);
        DB::table('input')->insert([
            'id' => 6,
            'name' => "x2",
            'label' => "To Coordinate X",
            'from_value' => 0,
            'to_value' => 0,
            'query_id' => 2,
        ]);
        DB::table('input')->insert([
            'id' => 7,
            'name' => "y1",
            'label' => "From Coordinate Y",
            'from_value' => 0,
            'to_value' => 0,
            'query_id' => 2,
        ]);
        DB::table('input')->insert([
            'id' => 8,
            'name' => "y2",
            'label' => "To Coordinate Y",
            'from_value' => 0,
            'to_value' => 0,
            'query_id' => 2,
        ]);
        DB::table('input')->insert([
            'id' => 9,
            'name' => "z1",
            'label' => "From Coordinate Z",
            'from_value' => 0,
            'to_value' => 0,
            'query_id' => 2,
        ]);
        DB::table('input')->insert([
            'id' => 10,
            'name' => "z2",
            'label' => "To Coordinate Z",
            'from_value' => 0,
            'to_value' => 0,
            'query_id' => 2,
        ]);
    }
}