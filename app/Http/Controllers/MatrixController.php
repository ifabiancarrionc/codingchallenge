<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Matrix;
use App\Models\Query;
use App\Models\MatrixQuery;
use App\Models\Block;
use App\Models\TestCase;
use Validator;
use DB;

class MatrixController extends Controller {

    public function save(Request $request) {
        $testCase = TestCase::findOrFail($request->get('test_case_id'));
        $data = $request->all();
        $validate = Validator::make($data, [
                    "test_case_id" => "required|exists:test_case,id",
                    "dimension" => "required|numeric|min:0|max:50",
                    "procedures_number" => "required|numeric|min:0|max:50",
        ]);
        if ($validate->fails() || $testCase->completed_attempts >= $testCase->attempts_number) {
            return redirect()->back()
                            ->withInput($request->all())
                            ->withErrors($validate->messages());
        } else {
            $data["completed_procedures"] = 0;
            $matrix = Matrix::create($data);
            $testCase = $matrix->testCase;
            $testCase->completed_attempts = $testCase->completed_attempts + 1;
            $testCase->save();
            return redirect('/matrix/' . $matrix->id);
        }
    }

    public function edit($id) {
        $matrix = Matrix::findOrFail($id);
        $queries = Query::get();
        return view('matrix.edit', ['matrix' => $matrix, 'queries' => $queries]);
    }

    public function update($id, Request $request) {
        $matrix = Matrix::findOrFail($id);
        $data = $request->all();
        $query = Query::findOrFail($data["query_id"]);
        $validate = $this->validateDynamicForm($data, $query, $matrix->dimension);
        if ($validate->fails() || $matrix->completed_procedures >= $matrix->procedures_number) {
            return redirect()->back()
                            ->withInput($request->all())
                            ->withErrors($validate->messages());
        } else {
            $this->{$query->machine_name . "Procedure"}($data, $query, $matrix);
            $matrix->completed_procedures = $matrix->completed_procedures + 1;
            $matrix->save();
            return redirect('/matrix/' . $matrix->id);
        }
    }

    protected function validateDynamicForm($data, $query, $dimension) {
        $validations = [];
        foreach ($query->inputs as $input) {
            $validations[$input->name] = 'required|numeric|min:' . ($input->from_value ? $input->from_value : 1) . '|max:' . ($input->to_value ? $input->to_value : $dimension);
        }
        return Validator::make($data, $validations);
    }

    protected function updateProcedure($data, $query, $matrix) {
        $block = Block::where('matrix_id', '=', $matrix->id)
                ->where('x', '=', $data['x'])
                ->where('y', '=', $data['y'])
                ->where('z', '=', $data['z'])
                ->first();
        if ($block) {
            $block->value = $data['value'];
            $block->save();
        } else {
            $data["matrix_id"] = $matrix->id;
            $block = Block::create($data);
        }
        $dataMatrixQuery["procedure"] = $data["x"] . ' ' . $data["y"] . ' ' . $data["z"] . ' ' . $data["value"];
        $dataMatrixQuery["result"] = "OK";
        $dataMatrixQuery["matrix_id"] = $matrix->id;
        $dataMatrixQuery["query_id"] = $query->id;
        $matrixQuery = MatrixQuery::create($dataMatrixQuery);
    }

    protected function queryProcedure($data, $query, $matrix) {
        $block = Block::select(DB::raw('SUM(value) as total'))
                ->where('matrix_id', '=', $matrix->id)
                ->where('x', '>=', $data['x1'])
                ->where('y', '>=', $data['y1'])
                ->where('z', '>=', $data['z1'])
                ->where('x', '<=', $data['x2'])
                ->where('y', '<=', $data['y2'])
                ->where('z', '<=', $data['z2'])
                ->first();
        $dataMatrixQuery["procedure"] = $data["x1"] . ' ' . $data["x2"] . ' ' . $data["y1"] . ' ' . $data["y2"] . ' ' . $data["z1"] . ' ' . $data["z2"];
        $dataMatrixQuery["result"] = $block->total?$block->total:0;
        $dataMatrixQuery["matrix_id"] = $matrix->id;
        $dataMatrixQuery["query_id"] = $query->id;
        $matrixQuery = MatrixQuery::create($dataMatrixQuery);
    }

}
