<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\TestCase;
use Validator;

class TestCaseController extends Controller {

    public function save(Request $request) {
        $data = $request->all();
        $validate = Validator::make($data, ["attempts_number" => "required|numeric|min:0|max:50"]);
        if ($validate->fails()) {
            return redirect('/testcase')
                            ->withInput($request->all())
                            ->withErrors($validate->messages());
        } else {
            $data["completed_attempts"] = 0;
            $testCase = TestCase::create($data);
            return redirect('/testcase/' . $testCase->id);
        }
    }

    function edit($id) {
        $testCase = TestCase::findOrFail($id);
        return view('testcase.edit', ['testcase' => $testCase]);
    }

    function index() {
        $testCases = TestCase::all();
        return view('testcase.index', ['testCases' => $testCases]);
    }

}
