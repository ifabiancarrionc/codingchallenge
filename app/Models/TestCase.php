<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TestCase extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'test_case';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['attempts_number', 'completed_attempts'];

    public function matrices() {
        return $this->hasMany('App\Models\Matrix');
    }
}