<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Block extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'block';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['x','y','z','value','matrix_id'];

    public function matrix() {
        return $this->belongsTo('App\Models\Matrix');
    }
}