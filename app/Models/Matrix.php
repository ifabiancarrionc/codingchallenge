<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Matrix extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'matrix';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['dimension','procedures_number', 'completed_procedures','test_case_id'];

    public function testCase() {
        return $this->belongsTo('App\Models\TestCase');
    }
    
    public function matrixQueries() {
        return $this->hasMany('App\Models\MatrixQuery');
    }
    
    public function blocks() {
        return $this->hasMany('App\Models\Block');
    }
}