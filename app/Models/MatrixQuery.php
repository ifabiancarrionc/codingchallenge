<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MatrixQuery extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'matrix_query';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['procedure','result','matrix_id','query_id'];

    public function matrix() {
        return $this->belongsTo('App\Models\Matrix');
    }
    
    public function queryObject() {
        return $this->belongsTo('App\Models\Query','query_id');
    }
}