<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Query extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'query';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name','machine_name'];

    public function matrixQueries() {
        return $this->hasMany('App\Models\MatrixQuery');
    }
    public function inputs() {
        return $this->hasMany('App\Models\Input');
    }
}