<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Input extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'input';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'label', 'from_value', 'to_value'];

    public function getQuery() {
        return $this->belongsTo('App\Models\Query');
    }

}
