<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('/', function () {
    return view('welcome');
});
Route::get('/testcase', 'TestCaseController@index');
Route::post('/testcase', 'TestCaseController@save');
Route::get('/testcase/{id}', 'TestCaseController@edit');
Route::post('/matrix', 'MatrixController@save');
Route::get('/matrix/{id}', 'MatrixController@edit');
Route::post('/matrix/{id}', 'MatrixController@update');
