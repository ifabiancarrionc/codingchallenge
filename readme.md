#Coding Challenge


You are given a 3-D Matrix in which each block contains 0 initially. The first block is defined by the coordinate (1,1,1) and the last block is defined by the coordinate (N,N,N). There are two types of queries.

+ UPDATE x y z W -- updates the value of block (x,y,z) to W.

+ QUERY -- x1 y1 z1 x2 y2 z2
calculates the sum of the value of blocks whose x coordinate is between x1 and x2 (inclusive), y coordinate between y1 and y2 (inclusive) and z coordinate between z1 and z2 (inclusive).


##Explanation

This project was developed on Laravel have an architecture MVC, then the layers are distributed on Model, Views and Controllers

+ Model: This is the manager of persistence and interactions with the app

    - /database/migrations: Contains the schema, with these files we create DB
    - /database/seeds: Contains the initial data, these will be stored in DB
    - /app/Models: These files are managers of interactions into DB and app

+ Controllers: The work for that layer is get or send information from or to views respectively, and same way with model

    - /app/Http/Controllers: We have two files, one for each principal entity, in these are all functions to information treatment, and communication with into views and models.

+ Views: This is the layer that show the information to user, and with forms send information that the user register on that.

    - /resources/views: In this folder we have the layout base for the project in this we can write modules statics and applicable for all screens, and the same quantity the folders how controllers we have, with views for send and get information of user.


##How install

+ Clone the project
+ Run command: php artisan key:generate
+ Run command: composer update
+ Run command: php artisan migrate --seed