@extends('base.base_layout')
@section('content')
<div class="row">
    <div class="box">
        <div class="col-lg-12">
            <hr>
            <h2 class="intro-text text-center">Test Case
            </h2>
            <hr>
            <div class="alert alert-{{ $testcase->completed_attempts < $testcase->attempts_number? 'warning':'success' }}">
                <strong>Attemps {{ $testcase->completed_attempts }} of  {{ $testcase->attempts_number }}</strong>
            </div>
            <br/>
            <?php $i = 1; ?>
            @foreach ($testcase->matrices as $matrix)
            <div class="alert alert-{{ $matrix->completed_procedures < $matrix->procedures_number? 'warning':'success' }}">
                <strong>Attemp #{{ $i }} - Matrix({{ $matrix->procedures_number.','.$matrix->dimension }}):</strong> {{ $matrix->completed_procedures < $matrix->procedures_number? 'Pending':'Completed' }} <a href="/matrix/{{ $matrix->id }}" class="alert-link">See more...</a>
            </div>
            <?php $i++; ?>
            @endforeach
            @if($testcase->completed_attempts < $testcase->attempts_number)
            <p>Insert matrix dimension and procedures numbers to use (between 1 to 50)</p>
            <form action="/matrix" method="post">
                {{ csrf_field() }}
                <input type="hidden" name="test_case_id" value="{{ old('testcase_id')?old('testcase_id'):$testcase->id }}"/>
                <div class="row">
                    <div class="form-group col-lg-4">
                        <label>Procedure Number</label>
                        <input type="number" class="form-control" name="procedures_number" min="1" max="50" required value="{{ old('procedures_number') }}"/>
                        {!! $errors->first("procedures_number") !!}
                    </div>
                    <div class="form-group col-lg-4">
                        <label>Dimension</label>
                        <input type="number" class="form-control" name="dimension" min="1" max="50" required value="{{ old('dimension') }}"/>
                        {!! $errors->first("dimension") !!}
                    </div>
                    <div class="form-group col-lg-12">
                        <input type="hidden" name="save" value="contact">
                        <button type="submit" class="btn btn-default">Submit</button>
                    </div>
                </div>
            </form>
            @else
            <p>You have exceeded the limit of attempts, do you want to create a <a href="/testcase" class="alert-link">new test case</a>?</p>
            @endif
        </div>
    </div>
</div>
@endsection