@extends('base.base_layout')
@section('content')
<div class="row">
    <div class="box">
        <div class="col-lg-12">
            <hr>
            <h2 class="intro-text text-center">Test Cases
            </h2>
            <hr>
            @foreach ($testCases as $testCase)
            <div class="alert alert-{{ $testCase->completed_attempts < $testCase->attempts_number? 'warning':'success' }}">
                <strong>Test Case #{{ $testCase->id }}:</strong> {{ $testCase->completed_attempts < $testCase->attempts_number? 'Pending':'Completed' }} <a href="/testcase/{{ $testCase->id }}" class="alert-link">See more...</a>
            </div>
            @endforeach
            <p>Insert attempts number that you want (between 1 to 50)</p>
            <form action="/testcase" method="post">
                <div class="row">
                    {{ csrf_field() }}
                    <div class="form-group col-lg-3">
                        <label>Attempts Number</label>
                        <input type="number" class="form-control" name="attempts_number" min="1" max="50" required value="{{ old('attempts_number') }}"/>
                    </div>
                    <div class="form-group col-lg-12">
                        <input type="hidden" name="save" value="contact">
                        <button type="submit" class="btn btn-default">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection