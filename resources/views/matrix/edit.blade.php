@extends('base.base_layout')
@section('content')
<div class="row">
    <div class="box">
        <div class="col-lg-12">
            <hr>
            <h2 class="intro-text text-center">Test Case
            </h2>
            <hr>
            <div class="alert alert-{{ $matrix->completed_procedures < $matrix->procedures_number? 'warning':'success' }}">
                <strong>Procedures {{ $matrix->completed_procedures }} of  {{ $matrix->procedures_number }}</strong>
            </div>
            <?php $i = 1; ?>
            @foreach ($matrix->matrixQueries as $matrixQuery)
            <div class="alert alert-success">
                <strong>Procedure #{{ $i }}:</strong> {{ $matrixQuery->procedure }} <strong>Result:</strong> {{ $matrixQuery->result }}
            </div>
            <?php $i++; ?>
            @endforeach
            @if ($matrix->completed_procedures < $matrix->procedures_number)
            <p>Select query to apply and fill the respective information</p>
            <div class="row">
                <div class="form-group col-lg-4">
                    <label>Query</label>
                    <select id="query_select" class="form-control">
                        <option value="">Select...</option>
                        @foreach ($queries as $query)
                        <option value="{{ $query->id }}" data-machine="{{ $query->machine_name }}">{{ $query->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            @foreach ($queries as $query)
            <form action="/matrix/{{ $matrix->id }}" method="post" style="display: none" id="{{ $query->machine_name }}" class="form">
                {{ csrf_field() }}
                <input type="hidden" name="query_id" value="{{ $query->id }}"/>
                <div class="row">
                    @foreach ($query->inputs as $input)
                    <div class="form-group col-lg-4">
                        <label>{{ $input->label }}</label>
                        <input type="number" class="form-control" name="{{ $input->name }}" min="{{ $input->from_value?$input->from_value:1 }}" max="{{ $input->to_value?$input->to_value:$matrix }}" required value="{{ old($input->name) }}"/>
                        {!! $errors->first($input->name) !!}
                    </div>
                    @endforeach
                </div>
                <div class="form-group col-lg-12">
                    <input type="hidden" name="save" value="contact">
                    <button type="submit" class="btn btn-default">Submit</button>
                </div>
            </form>
            @endforeach
            @else
            <p>You have exceeded the limit of procedures, do you want to create a <a href="/testcase/{{ $matrix->testCase->id }}" class="alert-link">new matrix</a>?</p>
            @endif
        </div>
    </div>
</div>
@endsection
@section('additional_js')
<script type="text/javascript" src="{{ asset('/js/jquery-3.1.1.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/main.js') }}"></script>
@endsection