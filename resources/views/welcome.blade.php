@extends('base.base_layout')
@section('content')
<div class="row">
    <div class="box">
        <div class="col-lg-12">
            <hr>
            <h2 class="intro-text text-center">Introduction
            </h2>
            <hr>
            <p>You are given a 3-D Matrix in which each block contains 0 initially. The first block is defined by the coordinate (1,1,1) and the last block is defined by the coordinate (N,N,N). There are two types of queries.</p>
            <ul>
                <li>"UPDATE x y z W" = updates the value of block (x,y,z) to W.</li>
                <li>"QUERY x1 x2 y1 y2 z1 z2" = calculates the sum of the value of blocks whose x coordinate is between x1 and x2 (inclusive), y coordinate between y1 and y2 (inclusive) and z coordinate between z1 and z2 (inclusive).</li>
            </ul>
            <div class="col-lg-12 text-center">
                <a href="/testcase" class="btn btn-default btn-lg">Let´s go</a>
            </div>
        </div>
    </div>
</div>
@endsection
